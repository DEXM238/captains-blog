<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Application\AppBundle\ApplicationAppBundle(),
			new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
			new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
			new Sonata\NewsBundle\SonataNewsBundle(),
			new Sonata\UserBundle\SonataUserBundle(),
			new Sonata\MediaBundle\SonataMediaBundle(),
			new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\SeoBundle\SonataSeoBundle(),
			new Sonata\IntlBundle\SonataIntlBundle(),
			new Sonata\FormatterBundle\SonataFormatterBundle(),
			new Sonata\ClassificationBundle\SonataClassificationBundle(),
			new FOS\UserBundle\FOSUserBundle(),
			new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
			new Knp\Bundle\MenuBundle\KnpMenuBundle(),
			new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
			new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
			new JMS\SerializerBundle\JMSSerializerBundle(),
            new Application\Sonata\NewsBundle\ApplicationSonataNewsBundle(),
            new Application\Sonata\UserBundle\ApplicationSonataUserBundle('FOSUserBundle'),
            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new Application\Sonata\ClassificationBundle\ApplicationSonataClassificationBundle(),
            new Misd\GuzzleBundle\MisdGuzzleBundle(),
            new Knp\Bundle\TimeBundle\KnpTimeBundle(),
            new Exercise\HTMLPurifierBundle\ExerciseHTMLPurifierBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
