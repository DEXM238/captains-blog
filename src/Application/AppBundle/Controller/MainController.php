<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 24.01.15
 * Time: 22:14
 */

namespace Application\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class MainController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('@ApplicationApp/Main/index.html.twig');
    }

}