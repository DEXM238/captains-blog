<?php
/**
 * Created by PhpStorm.
 * User: MM
 * Date: 06.12.2014
 * Time: 20:00
 */

namespace Application\AppBundle\Controller;

use Application\AppBundle\Form\Type\HashSumType;
use Guzzle\Http\Message\Header;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;


class ToolsController extends Controller
{
    /**
     * @Route("/hash")
     */
    public function hashAction(Request $request)
    {
        $form = $this->createForm(new HashSumType());
        $form->handleRequest($request);
        $hash = false;
        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            switch($data['algorithm']){
                case 'MD5':
                    $hash = md5($data['string']);
                    break;
                case 'SHA1':
                    $hash = sha1($data['string']);
                    break;
                default:
                    throw new HttpException(400);
            }
        }

        return $this->render('@ApplicationApp/Tools/hash.html.twig', [
            'form' => $form->createView(),
            'hash' => $hash,
        ]);
    }

    /**
     * @Route("/gzip")
     */
    public function checkGzipAction(Request $request)
    {
        $form = $this->createForm(new UrlType());
        $form->handleRequest($request);
        $isGzip = null;
        $headers = [
            'Accept-encoding' => 'gzip',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
        ];

        if($form->isSubmitted() && $form->isValid()){
            $client = $this->get('guzzle.client');
            $guzzleRequest = $client->createRequest('HEAD', $form->getData(), $headers);
            $res = $guzzleRequest->send();
            if($res->getRedirectCount() > 0){
                $guzzleRequest = $client->createRequest('HEAD', $res->getEffectiveUrl(), $headers);
                $res = $guzzleRequest->send();
            }
            /** @var Header $contentEncoding */
            $contentEncoding = $res->getHeader('Content-Encoding');
            $isGzip = ($contentEncoding) ? $contentEncoding->hasValue('gzip') : false;
        }

        return $this->render('@ApplicationApp/Tools/gzip.html.twig', [
            'form' => $form->createView(),
            'isGzip' => $isGzip,
        ]);
    }
} 