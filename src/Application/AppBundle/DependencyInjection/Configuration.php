<?php

namespace Application\AppBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('application_app');
        $rootNode
            ->children()
                ->arrayNode('wp_import')
                    ->children()
                        ->scalarNode('dbname')
                            ->defaultValue('default')
                        ->end()
                        ->scalarNode('user')
                            ->defaultValue('default')
                        ->end()
                        ->scalarNode('password')
                            ->defaultValue('default')
                        ->end()
                        ->scalarNode('driver')
                            ->defaultValue('default')
                        ->end()
                        ->scalarNode('host')
                            ->defaultValue('default')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
