<?php
/**
 * Created by PhpStorm.
 * User: MM
 * Date: 06.12.2014
 * Time: 20:02
 */
namespace Application\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class HashSumType extends AbstractType
{

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'hash_sum';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('string', 'textarea', ['trim' => false, 'constraints' => new NotBlank()])
            ->add('algorithm', 'choice', ['choices' => ['MD5' => 'MD5', 'SHA1' => 'SHA1'], 'constraints' => new NotBlank()])
        ;
    }
}