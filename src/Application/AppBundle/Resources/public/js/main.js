$(document).ready(function(){
    $('.scrollPagination').mCustomScrollbar({horizontalScroll: true, theme: "dark"});
    $('.search-icon, .search-close').on('click', function(e){
        e.preventDefault();
        $('.search-wrapper, .main-menu').toggleClass('hidden');
        if(!$('.search-wrapper').hasClass('hidden')){
            $('.search-wrapper input').focus();
        }
    });
});