<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 31.05.15
 * Time: 21:39
 */

namespace Application\AppBundle\Command;

use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\NewsBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\File\File;

class WPImportCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('wp:import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $params = $this->getContainer()->getParameter('application_app.wp_import');
        $cf = $this->getContainer()->get('doctrine.dbal.connection_factory');
        $connectionParams = [
            'dbname' => $params['dbname'],
            'user' => $params['user'],
            'password' => $params['password'],
            'host' => $params['host'],
            'driver' => $params['driver'],
        ];
        $conn = $cf->createConnection($connectionParams);

        $sql = "SELECT *
                FROM wp3_posts
                WHERE post_status = 'publish' AND post_type = 'post'";


        $stmt = $conn->query($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        $stmt->closeCursor();
        $conn->close();


        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $mediaManager = $this->getContainer()->get('sonata.media.manager.media');
        $purifier = $this->getContainer()->get('exercise_html_purifier.default');

        $author = $em->getRepository('ApplicationSonataUserBundle:User')->findOneById(1);

        foreach($rows as $row){
            $output->writeln($row['post_title']);

            $postContent = $purifier->purify($row['post_content']);

            $post = new Post();
            $post->setTitle($row['post_title']);
            $post->setEnabled(true);
            $post->setAbstract($row['post_title']);
            $post->setAuthor($author);
            $post->setRawContent($postContent);
            $post->setContent($postContent);
            $post->setContentFormatter('richhtml');
            $post->setCommentsDefaultStatus(2);
            $post->setCreatedAt(new \DateTime($row['post_date']));
            $post->setPublicationDateStart(new \DateTime($row['post_date']));
            $post->setSlug($row['post_name']);

            //extract first picture from post_content
            if(preg_match('/\<img .*?src=[\'\"](.*?)[\'\"]/ius', $row['post_content'], $imgMatch) && isset($imgMatch[1])){

                $image = new Media();
                $tmpImageName = getcwd().'/web/uploads/media/tmp/'.basename($imgMatch[1]);

                try {
                    file_put_contents($tmpImageName, file_get_contents($imgMatch[1]));
                    $file = new File($tmpImageName);
                    $image->setBinaryContent($file);
                    $image->setContext('default');
                    $image->setProviderName('sonata.media.provider.image');
                    $mediaManager->save($image);
                    $post->setImage($image);
                } catch(ContextErrorException $e){
                    $output->writeln('Download Error');
                }
            }

            $em->persist($post);
            $em->flush();
        }
        $em->clear();

        $output->writeln('WordPress');
    }

}