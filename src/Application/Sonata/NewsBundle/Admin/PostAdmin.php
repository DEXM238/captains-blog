<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 19.04.15
 * Time: 20:55
 */

namespace Application\Sonata\NewsBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\NewsBundle\Admin\PostAdmin as BasePostAdmin;

class PostAdmin extends BasePostAdmin {

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $commentClass = $this->commentManager->getClass();

        $formMapper
            ->with('General')
            ->add('enabled', null, ['required' => false])
            ->add('author', 'sonata_type_model_list')
            ->add('collection', 'sonata_type_model_list', ['required' => false])
            ->add('title')
            ->add('abstract', null, ['attr' => ['class' => 'span6', 'rows' => 5]])
//            ->add('rawContent', 'ckeditor', ['label' => 'label.content'])
            ->add('content', 'sonata_formatter_type', [
                'event_dispatcher' => $formMapper->getFormBuilder()->getEventDispatcher(),
                'format_field'   => 'contentFormatter',
                'source_field'   => 'rawContent',
                'source_field_options'      => array(
                    'attr' => array('class' => 'span10', 'rows' => 20)
                ),
                'target_field'   => 'content',
                'listener'       => true,
            ])
            ->add('image', 'sonata_media_type', array(
                'provider' => 'sonata.media.provider.image',
                'context'  => 'default'
            ))
            ->end()
            ->with('Tags')
            ->add('tags', 'sonata_type_model', [
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'label' => 'label.tags',
            ])
            ->end()
            ->with('Options')
            ->add('publicationDateStart')
            ->add('commentsCloseAt')
            ->add('commentsEnabled', null, ['required' => false])
            ->add('commentsDefaultStatus', 'choice', ['choices' => $commentClass::getStatusList(), 'expanded' => true])
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('author')
            ->add('enabled')
            ->add('title')
            ->add('abstract')
            ->add('content', null, ['safe' => true])
            ->add('tags')
            ->add('image')
        ;
    }

}