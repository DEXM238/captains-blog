<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 04.09.15
 * Time: 2:04
 */

namespace Application\Sonata\NewsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CommentFormCompilerPass implements CompilerPassInterface{

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('sonata.news.form.type.comment');
        $definition->setClass('Application\Sonata\NewsBundle\Form\Type\CommentType');
    }
}