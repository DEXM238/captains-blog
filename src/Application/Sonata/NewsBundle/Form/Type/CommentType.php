<?php

namespace Application\Sonata\NewsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Sonata\NewsBundle\Form\Type\CommentType as BaseType;

class CommentType extends BaseType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['attr' => ['placeholder' => 'form.comment.name'], 'label_attr' => ['class' => 'sr-only']])
            ->add('email', 'email', ['required' => false, 'attr' => ['placeholder' => 'form.comment.email'], 'label_attr' => ['class' => 'sr-only']])
            ->add('message', null, ['attr' => ['placeholder' => 'form.comment.message'], 'label_attr' => ['class' => 'sr-only']])
        ;
    }
}
