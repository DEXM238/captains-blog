<?php

/**
 * This file is part of the <name> project.
 *
 * (c) <yourname> <youremail>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Entity;

use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user_user")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin_uid", type="string", length=255)
     */
    protected $linkedinUid;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */
    protected $image;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $linkedinUid
     */
    public function setLinkedinUid($linkedinUid)
    {
        $this->linkedinUid = $linkedinUid;
    }

    /**
     * @return string
     */
    public function getLinkedinUid()
    {
        return $this->linkedinUid;
    }

    /**
     * @param MediaInterface $image
     */
    public function setImage(MediaInterface $image)
    {
        $this->image = $image;
    }

    /**
     * @return MediaInterface
     */
    public function getImage()
    {
        return $this->image;
    }
}