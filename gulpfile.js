var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var less = require('gulp-less');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

var env = process.env.GULP_ENV;

gulp.task('js', function(){
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
        'src/Application/AppBundle/Resources/public/js/*.js'])
        .pipe(concat('javascript.js'))
        .pipe(gulpif(env === 'prod', uglify()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/js'));
});

gulp.task('css', function(){
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
        'src/Application/AppBundle/Resources/public/css/*.less'])
        .pipe(gulpif(/[.]less/, less()))
        .pipe(concat('styles.css'))
        .pipe(autoprefixer())
        .pipe(gulpif(env === 'prod', uglifycss()))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('default', ['js', 'css']);